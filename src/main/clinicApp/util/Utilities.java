package main.clinicApp.util;

import java.time.LocalDate;

public class Utilities {

	private static int idLength = 11;
	private static int counter = 1;
	private static int padding = 5; 
	
	public static boolean verifyIdNumber(String idNumber) {
		if(idNumber.length() == idLength && Character.isLetter(idNumber.charAt(8))) {
			return true;
		}
		return false;
	}
	
	public static String generateMemberIdNumber(){
		LocalDate dateToday = LocalDate.now();
		String memberIdNumber = dateToday+""+counter;
		counter++;
		return memberIdNumber;
	}
}
