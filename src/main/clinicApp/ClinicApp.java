package main.clinicApp;
import java.io.IOException;

import main.controller.ClinicController;
import main.model.Patient;
import main.repository.Persistence;

public class ClinicApp {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		ClinicController controller = new ClinicController();	
		ClinicController.mainLoop();

	}

}
