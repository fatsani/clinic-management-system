package main.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.function.Predicate;

import main.model.Patient;

public class Persistence implements Storage{

	private  ArrayList<Patient> patientList = new ArrayList<>();


	private ArrayList<Patient> getPatientList() {
		return patientList;
	}

	private void setPatientList(ArrayList<Patient> patientList) {
		this.patientList = patientList;
	}

	@Override
	public void save(Object patient){
		ArrayList <Patient> temp = new ArrayList<>();
		try {
			File fil = new File("/home/fatsani/eclipse-workspace/ClinicManagementSystem/client.txt");
			
			if(fil.exists()) {
				temp = retrieveFromJson();
				temp.add((Patient) patient);
				
				FileOutputStream fs = new FileOutputStream("client.txt");
				ObjectOutputStream os = new ObjectOutputStream(fs);
				os.writeObject(temp);
	            os.close();
			}
			else{
				patientList.add((Patient) patient);
				FileOutputStream fs = new FileOutputStream("client.txt");
	            ObjectOutputStream os = new ObjectOutputStream(fs);
	            
	            os.writeObject(patientList);
	            os.close();
	            
			}
				System.out.println("Saved");     
        }
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
            e.printStackTrace();
        }
	}
	

	@Override
	public void delete(Object memberId) throws IOException{
		Patient patientToDelete = search((String) memberId);
		ArrayList <Patient> temp = new ArrayList<>();
		try {
			File fil = new File("/home/fatsani/eclipse-workspace/ClinicManagementSystem/client.txt");
			
			if(fil.exists()) {
				temp = retrieveFromJson();
				int patientIndex = indexNumber(patientToDelete, temp);

				temp.remove(patientIndex);
				
				FileOutputStream fs = new FileOutputStream("client.txt");
				ObjectOutputStream os = new ObjectOutputStream(fs);
				os.writeObject(temp);
	            os.close();
			}
			else{
				
			}
				System.out.println("True");
	            showPatientList(patientList);     
        }
		
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public void place(Object object, int index) throws IOException, ClassNotFoundException{
		ArrayList <Patient> temp = new ArrayList<>();
		try {
			File fil = new File("/home/fatsani/eclipse-workspace/ClinicManagementSystem/client.txt");
			
			if(fil.exists()) {
				temp = retrieveFromJson();
				temp.set(index,(Patient) object);
				
				FileOutputStream fs = new FileOutputStream("client.txt");
				ObjectOutputStream os = new ObjectOutputStream(fs);
				os.writeObject(temp);
	            os.close();
			}
			else{
				patientList.add((Patient)object);
				FileOutputStream fs = new FileOutputStream("client.txt");
	            ObjectOutputStream os = new ObjectOutputStream(fs);
	            
	            os.writeObject(patientList);
	            os.close();
	            
			}
				System.out.println("True");
			}
				
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
	            e.printStackTrace();
	        }
        }
	

	@Override
	public Patient search(String memberId) throws IOException {
		Patient temp = null;
		patientList = retrieveFromJson();
		for(Patient p : patientList) {
			if(p.getMemberId().equals(memberId)){
			temp = p;
			}
		}
		return temp;
	}

	public ArrayList<Patient> retrieveFromJson() throws IOException {
		ArrayList <Patient> result = new ArrayList <>();
		try{
		FileInputStream fis = new FileInputStream("client.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		result = (ArrayList<Patient>) ois.readObject();
		ois.close();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void showPatientList(ArrayList<Patient> pat) {
		for (Patient p : pat) {
			System.out.println(p);
		}
	}
	

	public boolean isIdUnique(String id) throws IOException {
		if(search(id)==null){
			return true;
		}
		else
		return false;
	}
	
	public int indexNumber(Patient patient, ArrayList<Patient> patientList) {
		int i = 0 ;	
		for( Patient p : patientList) {
				if (p.getMemberId().equals(patient.getMemberId())) {
					return i;
				}
			i++;
			}
		return i;
	}
	
}