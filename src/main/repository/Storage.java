package main.repository;

import java.io.IOException;

public interface Storage {
	
	void save(Object object);
	
	void delete(Object object) throws IOException;
	
	void place(Object object, int position) throws IOException, ClassNotFoundException;
	
	Object search(String searchParameter) throws IOException;
	
	

}
