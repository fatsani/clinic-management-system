package main.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Predicate;

import main.model.Patient;
import main.repository.Persistence;
import main.repository.Storage;


public class ClinicController {
		
	private static boolean running = true;
	
	private static String optionSelector = "0";
	
	private static Persistence storage = new Persistence();
	
	private static ClinicController controller = new ClinicController();
	
	//main menu loop
	public static void mainLoop() throws ClassNotFoundException, IOException {
		//Display Menu and get selection
		display("Please select a service you would like to use Test2");
		display("Option 1 : Create Patient \n"
				+ "Option 2 : Edit Patient\n"
				+ "Option 3 : Read Patient Records\n"
				+ "Option 4 : Delete Patient \n"
				+ "Option 5 : To exit");
		
		while(running) {
				
		optionSelector = (String) getInput();
		
		menuSystem(optionSelector);
	
		}
		display("Exiting Application\n"
				+ ".............\n"
				+ ". . . . . . .");
	}
	
	//call on displayer 
	
	public static void menuSystem(String optionSelector) throws ClassNotFoundException, IOException {
		switch(optionSelector) {
		case  "1":
			controller.createPatient();
			break;
		case  "2":
			controller.editPatient();
			break;
		case  "3":
			display("Read Patient Records");
			controller.viewRecords();
			
			break;
		case  "4":
			display("Delete Patient");
			controller.deletePatient();
			break;
		case  "5":
			running = false;
			break;
		default:
			display("Please select vaild option number");
		}
	}
	
	public static Object getInput() {
		Scanner input = new Scanner(System.in);
		return input.nextLine();
	}
	
	public static void display(Object objectToDisplay) {
		System.out.println(objectToDisplay);
	}
	
	public static boolean confirmCommand() {
		display("Please confirm your command, to confirm enter 1, to cancel enter 2");
		String command = null;
		command = (String) getInput();
		
		if(command.equals("1")) {
			return true;
		}
		else
			return false;
	}
	

	public void createPatient() throws ClassNotFoundException, IOException {
		display("Creat1e Patient\n"+ "Please enter the Patient details Test");
		display("Patient Name");
		String firstName = (String) getInput();
		display("Patient Surname");
		String lastName = (String) getInput();
		display("Patient ID Number");
		String memberId = (String) getInput();
		display("Patient Medical Society");
		String medicalAidSociety = (String) getInput();
		boolean isUnique = storage.isIdUnique(memberId);
		if(isUnique) {
			Patient p1 = new Patient(firstName, lastName, memberId, medicalAidSociety);
			storage.save(p1);
			display(p1);
		}else {
			display("The Id Number entered is not unique or not valid. Please search records for patient");
		}
		
	}
	
	
	public void editPatient() throws ClassNotFoundException, IOException {
		display("Edit Patient details\n"+ "Please enter the Patient details you would like to edit ");
		display("Patient MemberShip ID");
		String memberId = (String) getInput();
		ArrayList <Patient> result = storage.retrieveFromJson();
		Patient patientToEdit = storage.search(memberId); 
		int index = storage.indexNumber(storage.search(memberId),result); 
		display(patientToEdit);		
		display("Please enter the Patient details " );
		display("Patient Name");
		
		String firstName = (String) getInput();
		display("Patient Surname");
		String lastName = (String) getInput();
		display("Patient Medical Society");
		String medicalAidSociety = (String) getInput();
		patientToEdit.setFirstName(firstName);
		patientToEdit.setLastName(lastName);
		patientToEdit.setMedicalAidSociety(medicalAidSociety);
		storage.place(patientToEdit, index);
		display(patientToEdit);
	}
	
	
	public void viewRecords() throws IOException {
		display("To view a member please enter 1. To view all members enter 2");
		
		String input = (String) getInput();
		switch(input) {
		case  "1":
			display("Patient Member Id");
			
			String memberId = (String) getInput();
			
			controller.readPatientRecords(memberId);
			break;
		case  "2":
			controller.readPatientRecords();
			break;
		
		}
	}
	public void readPatientRecords() throws IOException {
		Persistence.showPatientList(storage.retrieveFromJson());
	}
	
	public void readPatientRecords(String memberId) throws IOException {
		display(storage.search(memberId));
	}
	
	public void deletePatient() throws IOException, ClassNotFoundException {
		display("Please enter the Patient details you would like to delete");
		display("Patient Name or MemberShip ID");
		String searchParam = (String) getInput();
		display(storage.search(searchParam)+ " will be deleted");
		if(confirmCommand()) {
			storage.delete(searchParam);
			display("Patient "+ searchParam +" has been deleted");
		}
		else 
			display("You have canceled the delete function");
	}
	 
	
}
