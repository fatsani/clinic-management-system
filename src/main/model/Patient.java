package main.model;

import java.io.Serializable;
import java.util.Scanner;

//import main.clinicApp.util.Utilities;

public class Patient  extends Person implements Serializable{
	private static final long serialVersionUID = 2072767247408945442L;
	private String memberId;
	private String medicalAidSociety;
	private String firstName;
	private String lastName; 
	
	public Patient(String firstName, String lastName, String memberId, String medicalAidSociety) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		//this.memberId = Utilities.generateMemberIdNumber();
		this.memberId = memberId;
		this.medicalAidSociety = medicalAidSociety;
	}

	public Patient() {
	}
	
	public String getInput() {
		Scanner sc = new Scanner(System.in);
		return sc.nextLine();
		}
	
	public String toString() {
		return "Name "+this.firstName +" "+ this.lastName +" Member ID# "+ this.memberId +" Medical Aid Society " + this.medicalAidSociety;
	}
	public String getMemberId() {
		return memberId;
	}
	
	public String getMedicalAidSociety() {
		return medicalAidSociety;
	}
	public void setMedicalAidSociety(String medicalAidSociety) {
		this.medicalAidSociety = medicalAidSociety;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || obj.getClass()!=this.getClass())
			return false;
		Patient patient = (Patient)obj;
		return (patient.getFirstName() == this.getFirstName() && patient.getMemberId() == this.getMemberId());
	}
}
